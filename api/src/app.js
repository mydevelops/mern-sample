const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')

const { jwt } = require('./common')
const apiRoutes = require('./routes/api')

const app = express()

app.use(cors())
app.use(jwt.configureExposeHeader)
app.use(logger('combined'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/apidoc', express.static(path.join(__dirname, 'apidoc')))

app.use('/api', apiRoutes)

// catch 404 and forward to error handler
app.use((req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'))
})

// error handler
app.use((err, req, res) => {
  const message = err.message
  const error = req.app.get('env') === 'development' ? err : {}

  res.status(err.status || 500).json({ message, error })
})

module.exports = app
