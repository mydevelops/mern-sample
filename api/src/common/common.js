const logger = require('../infrastructure/logger')
const Repository = require('../repository')
const Domain = require('../domain')
const config = require('../config')
const JTWAuth = require('../utils/JWTAuth')

class Common {
  constructor() {
    this.logger = logger(config.logger)

    const repository = new Repository(config.repository)
    this.domain = new Domain(repository)

    this.jwt = new JTWAuth(config.jwt)
  }
}

module.exports = new Common()
