module.exports = {
  general: require('./general'),
  logger: require('./logger'),
  repository: require('./repository'),
  jwt: require('./jwt'),
}
