module.exports = {
  header: 'token',
  secret: process.env.JWT_TOKEN || 'mySecret',
  options: {
    expiresIn: '168h',
  },
  refresh: true,
}
