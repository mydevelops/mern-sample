module.exports = {
  url: process.env.MONGO_URI || 'mongodb://localhost:27017/dev_notes_wall',
  options: {
    native_parser: true,
  },
  collections: {
    users: 'users',
    publications: 'publications',
  },
}
