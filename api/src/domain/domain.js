const UsersService = require('./users')
const PublicationsService = require('./publications')

class Domain {
  constructor(repository) {
    this.users = new UsersService(repository.users)
    this.publications = new PublicationsService(repository.publications)
  }
}

module.exports = Domain
