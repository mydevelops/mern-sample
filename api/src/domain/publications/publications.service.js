class PublicationsService {
  constructor(publicationsRepository) {
    this.publicationsRepository = publicationsRepository
  }

  findByFilter(filter = {}, pageNum, pageSize) {
    return this.publicationsRepository.findByFilter(filter, pageNum, pageSize)
  }

  create(_userId, userName, title, content) {
    return this.publicationsRepository.create(_userId, userName, title, content)
  }

  update(_id, _userId, userName, title, content) {
    return this.publicationsRepository.update(
      _id,
      _userId,
      userName,
      title,
      content,
    )
  }

  delete(_id, _userId) {
    return this.publicationsRepository.delete(_id, _userId)
  }
}

module.exports = PublicationsService
