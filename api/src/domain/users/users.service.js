const { hashPassword, comparePassword } = require('./users.utils')

class UsersService {
  constructor(usersRepository) {
    this.usersRepository = usersRepository
  }

  async signIn(name, password) {
    if (!name || !password) {
      throw new Error('user and password are mandatory')
    }

    const user = await this.usersRepository.findByName(name)
    await comparePassword(password, user.hash)
    return user
  }

  async signUp(name, password) {
    if (!name || !password) {
      throw new Error('name and password are mandatory')
    }

    const hash = await hashPassword(password)
    const user = await this.usersRepository.create(name, hash)
    return user
  }
}

module.exports = UsersService
