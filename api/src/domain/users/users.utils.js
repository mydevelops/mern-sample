const bcrypt = require('bcrypt-nodejs')

exports.hashPassword = password => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, null, null, (error, hash) => {
      if (error) {
        return reject(error)
      }

      resolve(hash)
    })
  })
}

exports.comparePassword = (password, hash) => {
  return new Promise((resolve, reject) =>
    bcrypt.compare(password, hash, (error, valid) => {
      if (error) {
        return reject(error)
      }

      if (!valid) {
        return reject(new Error('Invalid password.'))
      }

      resolve()
    }),
  )
}
