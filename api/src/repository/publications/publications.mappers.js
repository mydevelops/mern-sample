const ObjectID = require('mongoskin').ObjectID

exports.publicationDtoToModel = publication => ({
  ...publication,
  _id: publication._id.toString(),
  _userId: publication._userId.toString(),
})

exports.publicationModelToDto = publication => {
  const publicationDto = {
    ...publication,
  }

  if (publication._userId) {
    publicationDto._userId = new ObjectID(publication._userId)
  }

  if (publication._id) {
    publicationDto._id = new ObjectID(publication._id)
  }

  return publicationDto
}
