const {
  publicationModelToDto,
  publicationDtoToModel,
} = require('./publications.mappers')

const { buildFindQuery } = require('./publications.utils')

class PublicationsRepository {
  constructor(collection) {
    this.collection = collection
  }

  findByFilter(filter, pageNum = 0, pageSize = 10) {
    const query = buildFindQuery(filter)

    return new Promise((resolve, reject) => {
      this.collection
        .find(query)
        .sort({ createdAt: -1 })
        .limit(+pageSize)
        .skip(pageNum * pageSize)
        .toArray((error, publications) => {
          if (error) {
            return reject(error)
          }

          resolve(publications || [])
        })
    })
  }

  create(_userId, userName, title, content) {
    const publication = publicationModelToDto({
      _userId,
      userName,
      title,
      content,
      createdAt: new Date(),
    })

    return new Promise((resolve, reject) => {
      this.collection.insertOne(
        publication,
        (error, insertOneWriteOpResult) => {
          if (error) {
            return reject(error)
          }

          resolve(
            publicationDtoToModel({
              ...publication,
              _id: insertOneWriteOpResult.insertedId,
            }),
          )
        },
      )
    })
  }

  update(_id, _userId, userName, title, content) {
    const query = publicationModelToDto({ _id, _userId })

    const publication = publicationModelToDto({
      userName,
      title,
      content,
    })

    return new Promise((resolve, reject) => {
      this.collection.findOneAndUpdate(
        query,
        { $set: publication },
        { returnOriginal: false },
        (error, findAndModifyWriteOpResult) => {
          if (error) {
            return reject(error)
          }

          if (!findAndModifyWriteOpResult.value) {
            return reject(new Error('Publication not found'))
          }

          resolve(publicationDtoToModel(findAndModifyWriteOpResult.value))
        },
      )
    })
  }

  delete(_id, _userId) {
    const query = publicationModelToDto({ _id, _userId })

    return new Promise((resolve, reject) => {
      this.collection.deleteOne(query, (error, deleteWriteOpResult) => {
        if (error) {
          return reject(error)
        }

        if (!deleteWriteOpResult.result.n) {
          return reject(new Error('Publication not found'))
        }

        resolve()
      })
    })
  }
}

module.exports = PublicationsRepository
