const { publicationModelToDto } = require('./publications.mappers')

exports.buildFindQuery = (filter = {}) => {
  const { minDate, maxDate } = filter

  const query = removeUndefineds(
    publicationModelToDto({
      ...filter,
    }),
  )

  if (minDate) {
    query.createdAt = { $gte: minDate }
    delete query.minDate
  }

  if (maxDate) {
    query.createdAt = { $lte: maxDate }
    delete query.maxDate
  }

  return query
}

const removeUndefineds = obj => {
  const result = {}

  for (let key in obj) {
    if (obj[key] !== undefined) {
      result[key] = obj[key]
    }
  }

  return result
}
