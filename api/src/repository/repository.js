const db = require('mongoskin').db

const UsersRepository = require('./users')
const PublicationsRepository = require('./publications')

class Repository {
  constructor({ url, options, collections }) {
    this.db = db(url, options)

    this.users = new UsersRepository(this.db.collection(collections['users']))
    this.publications = new PublicationsRepository(
      this.db.collection(collections['publications']),
    )
  }
}

module.exports = Repository
