exports.userDtoToModel = user => ({
  ...user,
  _id: user._id.toString(),
})
