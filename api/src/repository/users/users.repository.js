const userDtoToModel = require('./users.mappers').userDtoToModel

class UsersRepository {
  constructor(collection) {
    this.collection = collection
  }

  findByName(name) {
    return new Promise((resolve, reject) => {
      this.collection.findOne({ name }, (error, user) => {
        if (error) {
          return reject(error)
        }

        if (!user) {
          return reject(new Error('User not found'))
        }

        resolve(userDtoToModel(user))
      })
    })
  }

  create(name, hash) {
    const user = { name, hash }

    return new Promise((resolve, reject) => {
      this.collection.insertOne(user, (error, insertOneWriteOpResult) => {
        if (error) {
          return reject(error)
        }

        resolve(
          userDtoToModel({
            ...user,
            _id: insertOneWriteOpResult.insertedId,
          }),
        )
      })
    })
  }
}

module.exports = UsersRepository
