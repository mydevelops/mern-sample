const express = require('express')
const router = express.Router()

const { jwt } = require('../../common')

const usersRouter = require('./users')
const publicationsRouter = require('./publications')

router.use('/users', usersRouter)

router.use(
  '/publications',
  jwt.handleToken,
  jwt.ensureAuthorized,
  publicationsRouter,
)

module.exports = router
