const { domain, jwt } = require('../../../common')
const { searchFilterDtoToModel } = require('./publications.mappers')

exports.searchPublicationHandler = async (req, res, next) => {
  const { _userId, userName, minDate, maxDate, page, num } = req.query

  const filter = searchFilterDtoToModel({ _userId, userName, minDate, maxDate })

  console.log(filter)

  try {
    const publications = await domain.publications.findByFilter(
      filter,
      page,
      num,
    )
    res.json({ data: publications })
  } catch (error) {
    next(error)
  }
}

exports.createPublicationHandler = async (req, res, next) => {
  const { title, content } = req.body

  if (!title || !content) {
    return next({
      status: 400,
      message: '"title" and "content" are mandatory.',
    })
  }

  const { userName, _userId } = jwt.getPayload(res)

  try {
    const publication = await domain.publications.create(
      _userId,
      userName,
      title,
      content,
    )
    res.json({ data: publication })
  } catch (error) {
    next(error)
  }
}

exports.modifyPublicationHandler = async (req, res, next) => {
  const { title, content } = req.body

  if (!title || !content) {
    return next({
      status: 400,
      message: '"title" and "content" are mandatory.',
    })
  }

  const { _id } = req.params
  const { userName, _userId } = jwt.getPayload(res)

  try {
    const publication = await domain.publications.update(
      _id,
      _userId,
      userName,
      title,
      content,
    )
    res.json({ data: publication })
  } catch (error) {
    next(error)
  }
}

exports.deletePublicationHandler = async (req, res, next) => {
  const { _id } = req.params
  const { _userId } = jwt.getPayload(res)

  try {
    await domain.publications.delete(_id, _userId)
    res.sendStatus(204)
  } catch (error) {
    next(error)
  }
}
