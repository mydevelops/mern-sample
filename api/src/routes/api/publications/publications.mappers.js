exports.searchFilterDtoToModel = searchFilterDto => {
  const searchFilterModel = {
    ...searchFilterDto,
  }

  if (searchFilterModel.minDate) {
    searchFilterModel.minDate = new Date(searchFilterModel.minDate)
  }

  if (searchFilterModel.maxDate) {
    searchFilterModel.maxDate = new Date(searchFilterModel.maxDate)
  }

  return searchFilterModel
}
