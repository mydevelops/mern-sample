const express = require('express')
const router = express.Router()

const {
  searchPublicationHandler,
  createPublicationHandler,
  modifyPublicationHandler,
  deletePublicationHandler,
} = require('./publications.handlers')

/**
 * @api {get} /api/publications Gets a list of publications
 * @apiName GetPublications
 * @apiGroup Publication
 * @apiHeader {String} token JWT token for user identification.
 *
 * @apiParam {String} [userName] User name (ex. "Carlos).
 * @apiParam {String} [minDate] Min publication date (ex. "2018-06-23T11:31:27.103Z").
 * @apiParam {String} [maxDate] Max publication date (ex. "2018-06-23T11:31:27.103Z").
 *
 * @apiSuccess (200) {Object[]} data Array of publications.
 * @apiSuccess (200) {String} data._id Unique publication identifier.
 * @apiSuccess (200) {String} data._userId Publicator unique identifier.
 * @apiSuccess (200) {String} data.userName Publicator user name.
 * @apiSuccess (200) {String} data.title Publication title.
 * @apiSuccess (200) {String} data.content Publication conten.
 * @apiSuccess (200) {String} data.createdAt Publication creation date (ex. 2018-08-06T16:48:58.344Z).
 *
 * @apiExample {curl} Example usage:
 *     curl -X GET \
 *       'http://localhost:3001/api/publications?userName=carlos&minDate=2018-06-23T11%3A31%3A27.103Z' \
 *       -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImNhcmxvcyIsIl91c2VySWQiOiI1YjJmNmZiNzAxNWI4YTQ3OWVjNWFmZjkiLCJpYXQiOjE1MzY2NzkxODMsImV4cCI6MTUzNzI4Mzk4M30.Sg3hvWmLaBEKgg_8hvdKzXXTu1HP-Iux41eF4kBhgGU'
 *
 * @apiSampleRequest /api/publications
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": [
 *         {
 *           "_id": "5b687bfa49be2625af1fd949",
 *           "_userId": "5b2f6fb7015b8a479ec5aff9",
 *           "userName": "carlos",
 *           "title": "qweqwqwe123asd",
 *           "content": "qweqqwes",
 *           "createdAt": "2018-08-06T16:48:58.344Z"
 *         },
 *         {
 *           "_id": "5b2f8960da6ae578b31e1e6a",
 *           "_userId": "5b2f6fb7015b8a479ec5aff9",
 *           "userName": "carlos",
 *           "title": "title 1",
 *           "content": "content 1",
 *           "createdAt": "2018-06-24T12:06:56.518Z"
 *         }
 *       ]
 *     }
 *
 * @apiError 401 Invalid token
 * @apiError 403 Insufficient permissions
 * @apiError 500 Server error
 */
router.get('/', searchPublicationHandler)

/**
 * @api {post} /api/publications Creates a publication
 * @apiName CreatePublication
 * @apiGroup Publication
 * @apiHeader {String} token JWT token for user identification.
 *
 * @apiParam {String} title Publication title (ex. "My Title").
 * @apiParam {String} content Publication content (ex. "My content").
 *
 * @apiSuccess (200) {Object} data Publication created.
 * @apiSuccess (200) {String} data._id Unique publication identifier.
 * @apiSuccess (200) {String} data._userId Publicator unique identifier.
 * @apiSuccess (200) {String} data.userName Publicator user name.
 * @apiSuccess (200) {String} data.title Publication title.
 * @apiSuccess (200) {String} data.content Publication conten.
 * @apiSuccess (200) {String} data.createdAt Publication creation date (ex. 2018-08-06T16:48:58.344Z).
 *
 * @apiExample {curl} Example usage:
 *     curl -X POST \
 *       http://localhost:3001/api/publications \
 *       -H 'content-type: application/x-www-form-urlencoded' \
 *       -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImNhcmxvcyIsIl91c2VySWQiOiI1YjJmNmZiNzAxNWI4YTQ3OWVjNWFmZjkiLCJpYXQiOjE1MzY2NzkxODMsImV4cCI6MTUzNzI4Mzk4M30.Sg3hvWmLaBEKgg_8hvdKzXXTu1HP-Iux41eF4kBhgGU' \
 *       -d 'title=title%201&content=content%201'
 *
 * @apiSampleRequest /api/publications
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *         "_id": "5b687bfa49be2625af1fd949",
 *         "_userId": "5b2f6fb7015b8a479ec5aff9",
 *         "userName": "carlos",
 *         "title": "qweqwqwe123asd",
 *         "content": "qweqqwes",
 *         "createdAt": "2018-08-06T16:48:58.344Z"
 *       }
 *     }
 *
 * @apiError 400 Invalid params
 * @apiError 401 Invalid token
 * @apiError 403 Insufficient permissions
 * @apiError 500 Server error
 */
router.post('/', createPublicationHandler)

/**
 * @api {put} /api/publications/:id Modifies a publication
 * @apiName ModifyPublication
 * @apiGroup Publication
 * @apiHeader {String} token JWT token for user identification.
 *
 * @apiParam {String} title Publication title (ex. "My Title").
 * @apiParam {String} content Publication content (ex. "My content").
 *
 * @apiSuccess (200) {Object} data Publication result.
 * @apiSuccess (200) {String} data._id Unique publication identifier.
 * @apiSuccess (200) {String} data._userId Publicator unique identifier.
 * @apiSuccess (200) {String} data.userName Publicator user name.
 * @apiSuccess (200) {String} data.title Publication title.
 * @apiSuccess (200) {String} data.content Publication conten.
 * @apiSuccess (200) {String} data.createdAt Publication creation date (ex. 2018-08-06T16:48:58.344Z).
 *
 * @apiExample {curl} Example usage:
 *     curl -X PUT \
 *       http://localhost:3001/api/publications/5b97e61627b8e248dad74f59 \
 *       -H 'content-type: application/x-www-form-urlencoded' \
 *       -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImNhcmxvcyIsIl91c2VySWQiOiI1YjJmNmZiNzAxNWI4YTQ3OWVjNWFmZjkiLCJpYXQiOjE1MzY2NzkxODMsImV4cCI6MTUzNzI4Mzk4M30.Sg3hvWmLaBEKgg_8hvdKzXXTu1HP-Iux41eF4kBhgGU' \
 *       -d 'title=title%201%20mod&content=content%201mod'
 *
 * @apiSampleRequest /api/publications
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *         "_id": "5b687bfa49be2625af1fd949",
 *         "_userId": "5b2f6fb7015b8a479ec5aff9",
 *         "userName": "carlos",
 *         "title": "qweqwqwe123asd",
 *         "content": "qweqqwes",
 *         "createdAt": "2018-08-06T16:48:58.344Z"
 *       }
 *     }
 *
 * @apiError 400 Invalid params
 * @apiError 401 Invalid token
 * @apiError 403 Insufficient permissions
 * @apiError 500 Server error
 */
router.put('/:_id', modifyPublicationHandler)

/**
 * @api {delete} /api/publications/:id Deletes a publication
 * @apiName DeletePublication
 * @apiGroup Publication
 * @apiHeader {String} token JWT token for user identification.
 *
 * @apiSuccess (204) NoBody No body
 *
 * @apiExample {curl} Example usage:
 *     curl -X DELETE \
 *     http://localhost:3001/api/publications/5b97e61627b8e248dad74f59 \
 *     -H 'content-type: application/x-www-form-urlencoded' \
 *     -H 'token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImNhcmxvcyIsIl91c2VySWQiOiI1YjJmNmZiNzAxNWI4YTQ3OWVjNWFmZjkiLCJpYXQiOjE1MzY2NzkxODMsImV4cCI6MTUzNzI4Mzk4M30.Sg3hvWmLaBEKgg_8hvdKzXXTu1HP-Iux41eF4kBhgGU'
 *
 * @apiSampleRequest /api/publications
 *
 * @apiError 401 Invalid token
 * @apiError 403 Insufficient permissions
 * @apiError 500 Server error
 */
router.delete('/:_id', deletePublicationHandler)

module.exports = router
