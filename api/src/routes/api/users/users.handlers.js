const { domain, jwt } = require('../../../common')

exports.signInHandler = async (req, res, next) => {
  let { name, pass } = req.body

  if (!name || !pass) {
    return next({
      status: 400,
      message: '"name" and "pass" are mandatory.',
    })
  }

  try {
    const user = await domain.users.signIn(name, pass)

    await jwt.setToken(res, generatePayload(user))

    res.sendStatus(204)
  } catch (error) {
    if (error.message === 'User not found') {
      error.status = 401
      error.message = 'Invalid user or password'
    }

    next(error)
  }
}

exports.signUpHandler = async (req, res, next) => {
  let { name, pass } = req.body

  if (!name || !pass) {
    return next({
      status: 400,
      message: '"name" and "pass" are mandatory.',
    })
  }

  try {
    const user = await domain.users.signUp(name, pass)

    await jwt.setToken(res, generatePayload(user))

    res.sendStatus(204)
  } catch (error) {
    next(error)
  }
}

const generatePayload = user => ({ userName: user.name, _userId: user._id })
