const express = require('express')
const router = express.Router()

const { signInHandler, signUpHandler } = require('./users.handlers')

/**
 * @api {post} /api/users/sign_in Signs in an user
 * @apiName SignIn
 * @apiGroup User
 *
 * @apiParam {String} name User name (ex. "Carlos").
 * @apiParam {String} pass User password (ex. "43211234").
 *
 * @apiSuccess (204) {String} token HEADER - Auth token
 *
 * @apiExample {curl} Example usage:
 *     curl -X POST \
 *       http://localhost:3001/api/users/sign_in \
 *       -H 'content-type: application/x-www-form-urlencoded' \
 *       -d 'name=carlos&pass=1234'
 *
 * @apiSampleRequest /api/users/sign_in
 *
 * @apiError 400 Invalid params
 * @apiError 500 Server error
 */
router.post('/sign_in', signInHandler)

/**
 * @api {post} /api/users/sign_up Signs up an user
 * @apiName SignUp
 * @apiGroup User
 *
 * @apiParam {String} name User name (ex. "Carlos").
 * @apiParam {String} pass User password (ex. "43211234").
 *
 * @apiSuccess (204) {String} token HEADER - Auth token
 *
 * @apiExample {curl} Example usage:
 *     curl -X POST \
 *       http://localhost:3001/api/users/sign_up \
 *       -H 'content-type: application/x-www-form-urlencoded' \
 *       -d 'name=carlos&pass=1234'
 *
 * @apiSampleRequest /api/users/sign_up
 *
 * @apiError 400 Invalid params
 * @apiError 401 Auth error
 * @apiError 500 Server error
 */
router.post('/sign_up', signUpHandler)

module.exports = router
