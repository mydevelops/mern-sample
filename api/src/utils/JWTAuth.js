const jwt = require('jsonwebtoken')

class JWTAuth {
  constructor({ header, secret, options = {} } = {}) {
    this.header = header || 'token'
    this.secret = secret || 'secret'
    this.options = {
      expiresIn: '1h',
      ...options,
    }

    this.configureExposeHeader = this.configureExposeHeader.bind(this)
    this.setToken = this.setToken.bind(this)
    this.ensureAuthorized = this.ensureAuthorized.bind(this)
    this.handleToken = this.handleToken.bind(this)
  }

  _sign(payload) {
    return new Promise((resolve, reject) => {
      jwt.sign(payload, this.secret, this.options, (error, token) => {
        if (error) {
          return reject(error)
        }

        resolve(token)
      })
    })
  }

  _verify(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, this.secret, this.options, (error, token) => {
        if (error) {
          return reject(error)
        }

        resolve(token)
      })
    })
  }

  configureExposeHeader(req, res, next) {
    res.set('Access-Control-Expose-Headers', this.header)

    next()
  }

  setToken(res, payload) {
    return this._sign(payload).then(token => res.set(this.header, token))
  }

  getPayload(res) {
    return res.locals.token
  }

  ensureAuthorized(req, res, next) {
    if (!res.locals.token) {
      return next({
        status: 403,
        message: 'Unauthorized',
      })
    }

    next()
  }

  handleToken(req, res, next) {
    const token = req.get(this.header)

    if (!token) {
      return next()
    }

    this._verify(token)
      .then(decoded => {
        const cleanedDecoded = this._cleanDecoded(decoded)

        res.locals.token = decoded

        if (!this.refresh) {
          return next()
        }

        return this._sign(cleanedDecoded).then(newToken => {
          res.set(this.header, newToken)
          next()
        })
      })
      .catch(err => {
        next({
          status: 403,
          message: err.message,
          error: err,
        })
      })
  }

  _cleanDecoded(decoded) {
    const cleanedDecoded = { ...decoded }

    delete cleanedDecoded.iat
    delete cleanedDecoded.exp

    return cleanedDecoded
  }
}

module.exports = JWTAuth
