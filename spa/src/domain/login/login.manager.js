import apiClient from '../../infrastructure/apiClient'

export const signUp = (name, pass) =>
  apiClient.post('users/sign_up', { name, pass })

export const signIn = (name, pass) =>
  apiClient.post('users/sign_in', { name, pass })
