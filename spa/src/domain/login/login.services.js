import * as loginRepository from './login.manager'
import sessionService from '../session'

export const signUp = async (name, pass) => {
  const result = await loginRepository.signUp(name, pass)
  sessionService.storeSession(result.headers.token)
}

export const signIn = async (name, pass) => {
  const result = await loginRepository.signIn(name, pass)
  sessionService.storeSession(result.headers.token)
}
