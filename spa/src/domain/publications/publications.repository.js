import apiClient from '../../infrastructure/apiClient'

export const findPublications = async (filter, token) => {
  const result = await apiClient.get('publications', filter, {
    headers: { token },
  })

  return result.data
}

export const createPublication = async (data, token) => {
  const result = await apiClient.post('publications', data, {
    headers: { token },
  })

  return result.data
}

export const editPublication = async (id, data, token) => {
  const result = await apiClient.put(`publications/${id}`, data, {
    headers: { token },
  })

  return result.data
}

export const deletePublication = async (id, token) => {
  const result = await apiClient.delete(`publications/${id}`, {
    headers: { token },
  })

  return result.data
}
