import * as publicationsRepository from './publications.repository'

import sessionService from '../session'

export const getPublications = (filter = {}) =>
  publicationsRepository.findPublications(
    filter,
    sessionService.retrieveSession(),
  )

export const createPublication = publication =>
  publicationsRepository.createPublication(
    publication,
    sessionService.retrieveSession(),
  )

export const editPublication = ({ _id, ...publication }) =>
  publicationsRepository.editPublication(
    _id,
    publication,
    sessionService.retrieveSession(),
  )

export const deletePublication = ({ _id }) =>
  publicationsRepository.deletePublication(
    _id,
    sessionService.retrieveSession(),
  )
