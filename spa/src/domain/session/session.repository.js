import { store, remove, retrieve } from '../../infrastructure/storage'

const TOKEN_KEY = 'token'

export const storeSession = token => store(TOKEN_KEY, token)

export const retrieveSession = () => retrieve(TOKEN_KEY)

export const removeSession = () => remove(TOKEN_KEY)
