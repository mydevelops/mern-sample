import { EventEmitter } from 'events'
import * as sessionRepository from './session.repository'

export const SESSION_UPDATED = 'sessionUpdated'

class SessionService extends EventEmitter {
  storeSession(token) {
    sessionRepository.storeSession(token)
    this.emit(SESSION_UPDATED, this.getSessionData())
  }

  retrieveSession() {
    return sessionRepository.retrieveSession()
  }

  removeSession() {
    sessionRepository.removeSession()
    this.emit(SESSION_UPDATED)
  }

  isLogged() {
    return !!sessionRepository.retrieveSession()
  }

  getSessionData() {
    return this.decodeSessionData(sessionRepository.retrieveSession())
  }

  decodeSessionData(token) {
    return token && JSON.parse(atob(token.split('.')[1]))
  }
}

export default new SessionService()
