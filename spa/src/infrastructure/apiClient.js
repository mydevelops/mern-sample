import * as axios from 'axios'

class ApiClient {
  urlBase = process.env.REACT_APP_API_URL

  async get(url, params, options) {
    const urlSearchParams = new URLSearchParams()
    for (let key in params) {
      urlSearchParams.append(key, params[key])
    }

    try {
      return await axios.get(
        `${this.urlBase}/${url}?${urlSearchParams.toString()}`,
        options,
      )
    } catch (error) {
      this._handleError(error)
    }
  }

  async post(url, params, options) {
    try {
      return await axios.post(`${this.urlBase}/${url}`, params, options)
    } catch (error) {
      this._handleError(error)
    }
  }

  async put(url, params, options) {
    try {
      return axios.put(`${this.urlBase}/${url}`, params, options)
    } catch (error) {
      this._handleError(error)
    }
  }

  async delete(url, options) {
    try {
      return await axios.delete(`${this.urlBase}/${url}`, options)
    } catch (error) {
      this._handleError(error)
    }
  }

  _handleError(error) {
    const newError = new Error(
      (error &&
        error.response &&
        error.response.data &&
        error.response.data.message) ||
        'An error has occurred',
    )
    newError.status = error && error.response && error.response.status

    throw newError
  }
}

export default new ApiClient()
