export const store = (key, value) => window.localStorage.setItem(key, value)

export const retrieve = key => window.localStorage.getItem(key)

export const remove = key => window.localStorage.removeItem(key)
