import React from 'react'
import { withRouter, Redirect } from 'react-router-dom'

import { signIn, signUp } from '../../domain/login'

import { withSession } from '../components/SessionProvider'

import LoginView from './Login.view'

class LoginContainer extends React.Component {
  handleSignUpSubmit = async ({ name, pass }) => {
    this.setState({ isLoading: true, errorText: '' })

    try {
      await signUp(name, pass)
      this.setState({ isLoading: false })
      this.props.history.push('/')
    } catch (error) {
      this.setState({ isLoading: false, errorText: error.message })
    }
  }

  handleSignInSubmit = async ({ name, pass }) => {
    this.setState({ isLoading: true, errorText: '' })

    try {
      await signIn(name, pass)
      this.setState({ isLoading: false })
      this.props.history.push('/')
    } catch (error) {
      this.setState({ isLoading: false, errorText: error.message })
    }
  }

  render() {
    return this.props.user ? (
      <Redirect to="/" />
    ) : (
      <LoginView
        {...this.state}
        onSignInSubmit={this.handleSignInSubmit}
        onSignUpSubmit={this.handleSignUpSubmit}
      />
    )
  }
}

export default withRouter(withSession(LoginContainer))
