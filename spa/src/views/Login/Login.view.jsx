import React from 'react'

import { Formik } from 'formik'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormControl from '@material-ui/core/FormControl'
import Button from '@material-ui/core/Button'
import FormHelperText from '@material-ui/core/FormHelperText'
import CircularProgress from '@material-ui/core/CircularProgress'

import AccountCircle from '@material-ui/icons/AccountCircle'
import Lock from '@material-ui/icons/Lock'
import { Typography } from '@material-ui/core'

const styles = theme => ({
  root: {
    flexGrow: 1,
    'margin-top': '50px',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  error: {
    color: 'red',
  },
})

const SUBMIT_TYPES = {
  SIGN_IN: 'singIn',
  SIGN_UP: 'signUp',
}
let submitType
const setSubmitType = type => (submitType = type)

const validateLoginForm = values => {
  const errors = {}

  if (!values.name) {
    errors.name = 'The name is mandatory'
  }

  if (!values.pass) {
    errors.pass = 'The password is mandatory'
  }

  return errors
}

const LoginView = ({
  classes,
  isLoading,
  errorText,
  onSignInSubmit,
  onSignUpSubmit,
}) => (
  <Grid container className={classes.root} justify="center">
    <Grid item xs={12} sm={8} md={6} lg={4}>
      <Grid
        container
        spacing={16}
        direction="column"
        alignItems="center"
        justify="center"
      >
        <Grid item>
          <Typography variant="display2">Log in</Typography>
        </Grid>
        <Formik
          onSubmit={values => {
            switch (submitType) {
              case SUBMIT_TYPES.SIGN_IN:
                onSignInSubmit(values)
                break
              case SUBMIT_TYPES.SIGN_UP:
                onSignUpSubmit(values)
                break
              default:
                break
            }
          }}
          validate={validateLoginForm}
          render={({
            values,
            errors,
            touched,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            submitForm,
          }) => (
            <form onSubmit={handleSubmit}>
              <Grid item>
                <FormControl
                  className={classes.margin}
                  error={Boolean(touched.name && errors.name)}
                >
                  <InputLabel htmlFor="name">Name</InputLabel>
                  <Input
                    id="name"
                    name="name"
                    type="name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    startAdornment={
                      <InputAdornment position="start">
                        <AccountCircle />
                      </InputAdornment>
                    }
                  />
                  {touched.name &&
                    errors.name && (
                      <FormHelperText>{errors.name}</FormHelperText>
                    )}
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl
                  className={classes.margin}
                  error={Boolean(touched.pass && errors.pass)}
                >
                  <InputLabel htmlFor="pass">Password</InputLabel>
                  <Input
                    id="pass"
                    name="pass"
                    type="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    startAdornment={
                      <InputAdornment position="start">
                        <Lock />
                      </InputAdornment>
                    }
                  />
                  {touched.pass &&
                    errors.pass && (
                      <FormHelperText>{errors.pass}</FormHelperText>
                    )}
                </FormControl>
              </Grid>
              {errorText && (
                <Grid item>
                  <Typography
                    className={classes.error}
                    variant="caption"
                    gutterBottom
                    align="center"
                  >
                    {errorText}
                  </Typography>
                </Grid>
              )}
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    setSubmitType(SUBMIT_TYPES.SIGN_UP)
                    submitForm()
                  }}
                  className={classes.button}
                  disabled={isLoading}
                >
                  Sign up
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    setSubmitType(SUBMIT_TYPES.SIGN_IN)
                    submitForm()
                  }}
                  className={classes.button}
                  disabled={isLoading}
                >
                  Sign in
                </Button>
                {isLoading && (
                  <CircularProgress className={classes.progress} size={20} />
                )}
              </Grid>
            </form>
          )}
        />
      </Grid>
    </Grid>
  </Grid>
)

export default withStyles(styles)(LoginView)
