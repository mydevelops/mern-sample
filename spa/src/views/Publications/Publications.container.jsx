import React from 'react'

import {
  getPublications,
  createPublication,
  editPublication,
  deletePublication,
} from '../../domain/publications'

import PublicationsView from './Publications.view'
import PublicationDialog from './components/PublicationDialog'
import ConfirmationDialog from './components/ConfirmationDialog'

class PublicationsContainer extends React.Component {
  state = {
    isPublicationDialogOpen: false,
    isDeletePublicationDialogOpen: false,
  }

  handleFilterChange = filterValues => {
    const { userName, createdAfter, createdBefore } = filterValues
    const filter = {}

    if (userName) {
      filter.userName = userName
    }

    if (createdAfter) {
      const [year, month, day] = createdAfter.split('-')
      console.log(createdAfter)
      filter.minDate = new Date(year, month - 1, day).toISOString()
    }

    if (createdBefore) {
      const [year, month, day] = createdBefore.split('-')
      filter.maxDate = new Date(year, month - 1, day, 23, 59, 59).toISOString()
    }

    this.setState({ filter }, this.loadPublications)
  }

  loadPublications = async () => {
    this.setState({ isLoading: true })

    try {
      const result = await getPublications(this.state.filter)
      this.setState({ publications: result.data, isLoading: false })
    } catch (error) {
      this.setState({ isLoading: false, error: error.message })
    }
  }

  componentDidMount() {
    this.loadPublications()
  }

  handleOnClickCreate = () =>
    this.setState({
      isPublicationDialogOpen: true,
      selectedPublication: undefined,
    })

  handleOnPublicationEdit = publication =>
    this.setState({
      isPublicationDialogOpen: true,
      selectedPublication: publication,
    })

  handleOnPublicationDelete = publication =>
    this.setState({
      isDeletePublicationDialogOpen: true,
      selectedPublication: publication,
    })

  handleOnClosePublicationDialog = () =>
    this.setState({ isPublicationDialogOpen: false })

  handleOnCloseDeletePublicationDialog = () =>
    this.setState({
      isDeletePublicationDialogOpen: false,
    })

  hadlePublicationDialogSubmit = async publication => {
    this.setState({
      isPublicationDialogOpen: false,
      selectedPublication: undefined,
    })

    await (publication._id
      ? editPublication(publication)
      : createPublication(publication))

    this.loadPublications()
  }

  handleDeletePublicationDialogSubmit = async publication => {
    this.setState({
      isDeletePublicationDialogOpen: false,
      selectedPublication: undefined,
    })

    await deletePublication(publication)

    this.loadPublications()
  }

  render() {
    return (
      <React.Fragment>
        <PublicationsView
          {...this.state}
          onFilterChange={this.handleFilterChange}
          onClickCreate={this.handleOnClickCreate}
          onClickEdit={this.handleOnPublicationEdit}
          onClickDelete={this.handleOnPublicationDelete}
        />

        {this.state.isPublicationDialogOpen && (
          <PublicationDialog
            publication={this.state.selectedPublication}
            onClose={this.handleOnClosePublicationDialog}
            onSubmit={this.hadlePublicationDialogSubmit}
            open
          />
        )}
        <ConfirmationDialog
          publication={this.state.selectedPublication}
          onClose={this.handleOnCloseDeletePublicationDialog}
          onConfirm={this.handleDeletePublicationDialogSubmit}
          open={this.state.isDeletePublicationDialogOpen}
        />
      </React.Fragment>
    )
  }
}

export default PublicationsContainer
