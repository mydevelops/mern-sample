import React from 'react'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'

import Filter from './components/Filter'

import PublicationsList from './components/PublicationsList'

const styles = them => ({
  button: { margin: '10px' },
})

const PublicationsViewRaw = ({
  isLoading,
  error,
  publications,
  onFilterChange,
  onClickCreate,
  onClickEdit,
  onClickDelete,
  classes,
}) => (
  <React.Fragment>
    <Filter onChange={filterValues => onFilterChange(filterValues)} />

    <Button
      className={classes.button}
      variant="contained"
      color="primary"
      size="large"
      onClick={onClickCreate}
    >
      Create new publication
    </Button>

    {error && <h3>An error has happen: {error}</h3>}
    {isLoading ? (
      <h3>Loading publications...</h3>
    ) : publications && publications.length ? (
      <PublicationsList
        publications={publications}
        {...{ onClickEdit, onClickDelete }}
      />
    ) : (
      <h3>Cannot find any publication</h3>
    )}
  </React.Fragment>
)

export default withStyles(styles)(PublicationsViewRaw)
