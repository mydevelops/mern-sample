import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import blue from '@material-ui/core/colors/blue'
import Button from '@material-ui/core/Button'

const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: { padding: 10 },
}

class PublicationDialogRaw extends React.Component {
  handleClose = () => {
    this.props.onClose()
  }

  handleConfirm = () => {
    this.props.onConfirm(this.props.publication)
  }

  render() {
    const { classes, publication, ...other } = this.props

    return (
      <Dialog
        onClose={this.handleClose}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <DialogTitle id="simple-dialog-title">Delete publication</DialogTitle>
        <div className={classes.container}>
          <div>Are you sure?</div>

          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleConfirm}
            >
              Delete
            </Button>
            <Button onClick={this.handleClose}>Cancel</Button>
          </div>
        </div>
      </Dialog>
    )
  }
}

export default withStyles(styles)(PublicationDialogRaw)
