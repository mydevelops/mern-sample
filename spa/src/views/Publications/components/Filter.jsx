import React from 'react'

import * as _ from 'underscore'

import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
})

class FilterRaw extends React.Component {
  constructor(...args) {
    super(...args)

    this.state = this.initialState

    this.onFieldChange = this.onFieldChange.bind(this)
    this.clear = this.clear.bind(this)
    this.handleOnChange = _.debounce(() => this.props.onChange(this.state), 300)
  }

  get initialState() {
    return {
      userName: '',
      createdAfter: '',
      createdBefore: '',
    }
  }

  onFieldChange(event) {
    this.setState({ [event.target.name]: event.target.value }, () =>
      this.handleOnChange(),
    )
  }

  clear() {
    this.setState(this.initialState, () => this.props.onChange(this.state))
  }

  render() {
    const { classes } = this.props

    return (
      <div className={classes.container}>
        <TextField
          label="User name"
          type="text"
          name="userName"
          className={classes.textField}
          margin="normal"
          onChange={this.onFieldChange}
          value={this.state.userName}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label="Created after"
          type="date"
          name="createdAfter"
          className={classes.textField}
          margin="normal"
          onChange={this.onFieldChange}
          value={this.state.createdAfter}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label="Created before"
          type="date"
          name="createdBefore"
          className={classes.textField}
          margin="normal"
          onChange={this.onFieldChange}
          value={this.state.createdBefore}
          InputLabelProps={{
            shrink: true,
          }}
        />

        <Button
          variant="contained"
          className={classes.button}
          onClick={this.clear}
        >
          Clear filter
        </Button>
      </div>
    )
  }
}

export default withStyles(styles)(FilterRaw)
