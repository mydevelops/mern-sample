import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import blue from '@material-ui/core/colors/blue'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import { DialogContent, DialogActions } from '@material-ui/core'

const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: { padding: 10 },
}

class PublicationDialogRaw extends React.Component {
  state = {
    publication: this.props.publication || { title: '', content: '' },
  }

  handleClose = () => {
    this.props.onClose()
  }

  handleSubmit = () => {
    this.setState({ error: '' })

    const { title, content } = this.state.publication

    if (!title) {
      return this.setState({ error: 'Title is mandatory' })
    }

    if (!content) {
      return this.setState({ error: 'Content is mandatory' })
    }

    this.props.onSubmit(this.state.publication)
  }

  onFieldChange = event => {
    this.setState({
      publication: {
        ...this.state.publication,
        [event.target.name]: event.target.value,
      },
    })
  }

  render() {
    const { classes, publication, ...other } = this.props

    return (
      <Dialog
        onClose={this.handleClose}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <DialogTitle id="simple-dialog-title">
          {publication && publication._id ? 'Modify' : 'Create'} publication
        </DialogTitle>
        <DialogContent>
          <form className={classes.container} noValidate autoComplete="off">
            <div>
              <TextField
                autoFocus
                id="title"
                label="Title"
                name="title"
                value={this.state.publication.title}
                onChange={this.onFieldChange}
                margin="normal"
                fullWidth
              />
            </div>
            <div>
              <TextField
                id="content"
                label="Content"
                name="content"
                value={this.state.publication.content}
                onChange={this.onFieldChange}
                margin="normal"
                fullWidth
              />
            </div>
            {this.state.error && <p>{this.state.error}</p>}
          </form>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSubmit}
          >
            {publication && publication._id ? 'Modify' : 'Create'}
          </Button>
          <Button onClick={this.handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default withStyles(styles)(PublicationDialogRaw)
