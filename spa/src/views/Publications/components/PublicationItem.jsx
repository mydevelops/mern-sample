import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import { withSession } from '../../components/SessionProvider'

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  padding: {
    padding: 10,
  },
}

const CardRaw = ({
  publication,
  classes,
  onClickEdit,
  onClickDelete,
  user,
}) => (
  <div className={classes.padding}>
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary">
          {publication.userName}
        </Typography>
        <Typography variant="headline" component="h2">
          {publication.title}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {new Date(publication.createdAt).toLocaleString()}
        </Typography>
        <Typography component="p">{publication.content}</Typography>
      </CardContent>
      {publication._userId === user._userId && (
        <CardActions>
          <Button size="small" onClick={() => onClickEdit(publication)}>
            Edit
          </Button>
          <Button size="small" onClick={() => onClickDelete(publication)}>
            Delete
          </Button>
        </CardActions>
      )}
    </Card>
  </div>
)

export default withStyles(styles)(withSession(CardRaw))
