import React from 'react'

import PublicationItem from './PublicationItem'

export default ({ publications, onClickEdit, onClickDelete }) => (
  <React.Fragment>
    {publications &&
      publications.length &&
      publications.map(publication => (
        <PublicationItem
          key={publication._id}
          publication={publication}
          {...{ onClickEdit, onClickDelete }}
        />
      ))}
  </React.Fragment>
)
