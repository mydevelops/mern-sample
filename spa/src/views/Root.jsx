import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import { PrivateRoute } from './components/PrivateRoute'
import Header from './components/Header'
import { SessionProvider } from './components/SessionProvider'

import Login from './Login'
import Publications from './Publications'

export default () => (
  <SessionProvider>
    <Header />
    <Switch>
      <Redirect exact path="/" to="/publications" />
      <Route path="/login" component={Login} />
      <PrivateRoute path="/publications" component={Publications} />
    </Switch>
  </SessionProvider>
)
