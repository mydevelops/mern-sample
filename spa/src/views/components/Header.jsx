import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { withSession } from './SessionProvider'

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
}

const Header = ({ classes, user, closeSession }) => (
  <div className={classes.root}>
    <AppBar position="static">
      <Toolbar>
        <Typography variant="title" color="inherit" className={classes.flex}>
          MERN-SAMPLE
        </Typography>
        {user && (
          <Button color="inherit" onClick={closeSession}>
            Log out
          </Button>
        )}
      </Toolbar>
    </AppBar>
  </div>
)

export default withStyles(styles)(withSession(Header))
