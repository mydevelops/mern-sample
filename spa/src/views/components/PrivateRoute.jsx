import React from 'react'

import { Route, Redirect } from 'react-router-dom'

import { withSession } from './SessionProvider'

const PrivateRouteRaw = ({ user, ...props }) =>
  user ? <Route {...props} /> : <Redirect to="/login" />

export const PrivateRoute = withSession(PrivateRouteRaw)
