import React from 'react'
import {
  default as sessionService,
  SESSION_UPDATED,
} from '../../domain/session'

const SessionContext = React.createContext({
  user: undefined,
  closeSession: () => {},
})

export class SessionProvider extends React.Component {
  state = {
    user: sessionService.getSessionData(),
    closeSession: () => sessionService.removeSession(),
  }

  constructor(...args) {
    super(...args)

    this.handleSessionChange = this.handleSessionChange.bind(this)
  }

  componentDidMount() {
    sessionService.on(SESSION_UPDATED, this.handleSessionChange)
  }

  componentWillUnmount() {
    sessionService.removeListener(SESSION_UPDATED, this.handleSessionChange)
  }

  handleSessionChange(user) {
    this.setState({ user })
  }

  render() {
    return (
      <SessionContext.Provider value={this.state}>
        {this.props.children}
      </SessionContext.Provider>
    )
  }
}

export const withSession = WrappedComponent => props => (
  <SessionContext.Consumer>
    {session => <WrappedComponent {...session} {...props} />}
  </SessionContext.Consumer>
)
